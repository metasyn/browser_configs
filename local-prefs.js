\\
lockPref("browser.startup.homepage", "education.ucsb.edu");
lockPref("browser.rights.3.shown", true);
lockPref("update_notifications.enabled", false);
lockPref("browser.shell.checkDefaultBrowser", true);
lockpref("app.update.showInstalledUI", false);
lockPref("app.update.silent", true);
lockpref("app.update.mode", 0);
pref("extensions.update.autoUpdateDefault", true);