Browser Configurations
======================

Let's configure the browsers. This should've been done a while ago. Or on the image.

Chrome
------
1. master_preferences
    + Put in `C:\Program Files (x86)\Google\Chrome\Application`


Firefox
-------

1. mozilla.cfg
    + Put in `C:\Program Files (x86)\Mozilla Firefox`
2. local-prefs.js & channel-prefs.js
    + Put in `C:\Program Files (x86)\Mozilla Firefox\defaults\pref`
